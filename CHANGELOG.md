### 18.0.0 Update to Angular 18.
* 23d4f3c -- [CI/CD] Update packages.json version based on GitLab tag.
* 73c0c28 -- Merge branch 'develop' into 'master'
* 27f3213 -- Merge branch '8-update-to-angular-18' into 'develop'
* 8a9e506 -- Update to Angular 18
### 16.0.1 Update sentinel versions.
* c7979eb -- [CI/CD] Update packages.json version based on GitLab tag.
* 53e36be -- Merge branch 'update-sentinel-versions' into 'master'
* a5584ec -- Update sentinel versions.
### 16.0.0 Update to Angular 16 and update local issuer to keycloak.
* c8331d2 -- [CI/CD] Update packages.json version based on GitLab tag.
*   0450a63 -- Merge branch '6-update-to-angular-16' into 'master'
|\  
| * cac740e -- Update to keycloak auth provider, refactor minor setting
| * 3f15452 -- Update to Angular 16
|/  
* 75de35a -- [CI/CD] CHANGELOG.md file updated with commits between the current and previous tag.
* 9816ff1 -- [CI/CD] Update packages.json version based on GitLab tag.
* 2e4fa52 -- Update to Angular 15
* a358225 -- Update lint rules and mock service
* 0f78cbf -- Prettify the code
* dcbae84 -- Fix chart display delay
* 110248e -- Add new mocking service
* 4986b5a -- Merge branch '4-fix-axes-tooltip' into 'master'
* e34126b -- Resolve "Fix axes tooltip"
### 15.0.0 Update to Angular 15
* a66aef7 -- [CI/CD] Update packages.json version based on GitLab tag.
*   a781a63 -- Merge branch '5-add-local-mock-data-for-static-deployed-demonstration' into 'master'
|\  
| * e8b6977 -- Update to Angular 15
* | e835632 -- Merge branch '5-add-local-mock-data-for-static-deployed-demonstration' into 'master'
|\| 
| * a358225 -- Update lint rules and mock service
| * 0f78cbf -- Prettify the code
| * dcbae84 -- Fix chart display delay
| * 110248e -- Add new mocking service
|/  
* 4986b5a -- Merge branch '4-fix-axes-tooltip' into 'master'
* e34126b -- Resolve "Fix axes tooltip"
### 14.0.2 Propagate insufficient data info for dashboard processing
* 864fbd0 -- [CI/CD] Update packages.json version based on GitLab tag.
* 443a0c3 -- Merge branch '3-indicate-insufficient-data' into 'master'
* 84dc3d3 -- Add control output for dashboard chart display
* 5cd0551 -- Add check for missing data
* 6b652ed -- Add check for NaN and 0
* cd88e95 -- Initial methods for check
### 14.0.1 Update API routes
* e586efc -- [CI/CD] Update packages.json version based on GitLab tag.
* 2e4113e -- Merge branch '2-fix-api-calls' into 'master'
* d018162 -- Update API routes
* 2f30118 -- Fix API calls and json-server routes
### 14.0.0 Initial clustering library version
